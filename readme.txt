Crossover - Trial Project
 
Assignment 1
 
The basic requirement of the challenge was read all properties contained in files of different formats, from different data sources and still return the sorted properties alphabetically.
Considering all these factors, it turns out to be a relatively complex problem.
Several techniques were used to solve the challenge, such as regular expressions and also the application of different design patterns.
To ensure code quality, various automated testing were implemented.
Overall, I found the challenge very interesting.